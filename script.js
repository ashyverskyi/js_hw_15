/* Теоритичні питання:
1. В чому відмінність між setInterval та setTimeout?
   setTimeout используется для отложенного выполнения функции один раз, а setInterval - для повторного выполнения функции через определенный интервал до его очистки.

2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
   Для того чтобы остановить выполнение функции, которая была запланирована для вызова с помощью setTimeout или setInterval, нужно использовать функции clearTimeout и clearInterval соответственно.

Практичне завдання 1: 

-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати,
 що операція виконана успішно.

Практичне завдання 2: 

Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. 
Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/

// TASK 1

document.getElementById("connect").addEventListener("click", function () {
    let divElement = document.getElementById("serverName");
    divElement.textContent = "Connecting...";

    setTimeout(function () {
        divElement.innerText = "Connection successful";
        countdown();
    }, 3000);
});

// TASK 2

function countdown() {
    let countdownElement = document.getElementById("countdown");
    let count = 10;
    let timer = setInterval(function () {
        countdownElement.textContent = count;
        count--;
        if (count < 0) {
            clearInterval(timer);
            document.getElementById("serverName").innerText = "Connection timeout. Try again.";
        }
    }, 1000);
}

